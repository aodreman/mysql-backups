#! /bin/bash
# BD backup script
echo "Creating the backups for you, my Master!"
# Variables
# user and password should be setup in ~/.my.cnf

BACKUP_DIR=~/mysql-backups/$(/bin/date +\%Y-\%m-\%d);
test -d "$BACKUP_DIR" || mkdir -p "$BACKUP_DIR"
# Get the database list, exclude information_schema

for db in $(mysql -B -s -e 'show databases' | grep -v -e "information_schema" -e "performance_schema" -e "mysql")
do
	# dump each database in a separate file
	mysqldump "$db" | gzip > "$BACKUP_DIR/$db.sql.gz"
done
