#! /bin/bash
# Script para los backups de las BD de mySQL en ws01.logoscorp.com
echo "Creating the backups for you, my Master!"
# Variables
MYSQL_USER="xxx_user"
MYSQL_PASS="xxx_password"
BACKUP_DIR=/home/server/mysql_dumps/$(/bin/date +\%Y-\%m-\%d);
test -d "$BACKUP_DIR" || mkdir -p "$BACKUP_DIR"
# Get the database list, exclude information_schema
for db in $(mysql -B -s -u $MYSQL_USER --password=$MYSQL_PASS -e 'show databases' | grep -v -e "information_schema" -e "performance_schema" -e "mysql")
do
	# dump each database in a separate file
	mysqldump -u $MYSQL_USER --password=$MYSQL_PASS "$db" | gzip > "$BACKUP_DIR/$db.sql.gz"
done
